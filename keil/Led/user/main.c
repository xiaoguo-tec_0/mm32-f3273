#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"

/**
 * @brief 软件延时函数
 * 
 * @param x 延时时间
 */
void APP_Delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 10000u; j++)
        {
            __NOP();
        }
    }
}

void LED_Init(void)
{
    /* gpio. */
    GPIO_Init_Type gpio_init;

    gpio_init.Pins = GPIO_PIN_1;
    gpio_init.PinMode = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio_init);
    GPIO_WriteBit(GPIOA, gpio_init.Pins, 1u);
    GPIO_PinAFConf(GPIOA, gpio_init.Pins, GPIO_AF_15); /* disable the alternative functions. */
}

void LED_On(void)
{
    GPIO_WriteBit(GPIOA, GPIO_PIN_1, 0u);
}

void LED_Off(void)
{
    GPIO_WriteBit(GPIOA, GPIO_PIN_1, 1u);
}	

int main(void)
{
	BOARD_InitBootClocks();  // 初始化时钟
	LED_Init();
	while(1)
	{
		LED_On();
		APP_Delay(500);
		LED_Off();
		APP_Delay(500);
	}	
}

