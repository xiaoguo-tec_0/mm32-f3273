#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"

#define NVIC_PRIORITYGROUP_0         0x00000007U /*!< 0 bits for pre-emption priority
                                                      4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         0x00000006U /*!< 1 bits for pre-emption priority
                                                      3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         0x00000005U /*!< 2 bits for pre-emption priority
                                                      2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         0x00000004U /*!< 3 bits for pre-emption priority
                                                      1 bits for subpriority */
#define NVIC_PRIORITYGROUP_4         0x00000003U /*!< 4 bits for pre-emption priority
                                                      0 bits for subpriority */

/**
 * @brief 软件延时函数
 * 
 * @param x 延时时间
 */
void APP_Delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 10000u; j++)
        {
            __NOP();
        }
    }
}


int main(void)
{
	BOARD_InitBootClocks();  // 初始化时钟
	BOARD_InitDebugConsole();
	
	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4); //设置中断分组
	
	#if SYSTICK_MODE == USE_INTERRUPT_MODE
	BOARD_Systick_Init(CLOCK_GetBootHSEValue() / 1000);
	#elif SYSTICK_MODE == USE_HAL_STYLE_MODE
	Drv_SysTick_Config();
	#endif

	LED_Init();
	printf("Board Init Success\r\n");
	while(1)
	{
		LED_On();
		HAL_Delay(1000);
		LED_Off();
		HAL_Delay(1000);
	}	
}

