#ifndef __SYSTICK_H__
#define __SYSTICK_H__
#include "hal_common.h"

#define USE_INTERRUPT_MODE  	0
#define USE_POLL_DIV8_MODE		1  // 8Div
#define USE_POLL_DIV1_MODE		2  // NO DIV
#define USE_HAL_STYLE_MODE		3  // ��stm32Hal��

#define SYSTICK_MODE		USE_HAL_STYLE_MODE


void BOARD_Delay1Us(uint32_t count);
void BOARD_Delay1Ms(uint32_t count);

#if SYSTICK_MODE == USE_INTERRUPT_MODE
uint32_t BOARD_Systick_Init(uint32_t ticks);
#elif SYSTICK_MODE == USE_HAL_STYLE_MODE
void HAL_Delay(unsigned int Delay);
unsigned int HAL_GetTick(void);
int Drv_SysTick_Config(void);
#endif
#endif