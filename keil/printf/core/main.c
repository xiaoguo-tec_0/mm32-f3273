#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"

/**
 * @brief 软件延时函数
 * 
 * @param x 延时时间
 */
void APP_Delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 10000u; j++)
        {
            __NOP();
        }
    }
}


int main(void)
{
	BOARD_InitBootClocks();  // 初始化时钟
	BOARD_InitDebugConsole();
	LED_Init();
	while(1)
	{
		LED_On();
		printf("led on\r\n");
		APP_Delay(1000);
		LED_Off();
		printf("led off\r\n");
		APP_Delay(1000);
	}	
}

