#include "led.h"

void LED_Init(void)
{
    /* gpio. */
    GPIO_Init_Type gpio_init;

    gpio_init.Pins = USER_LED_PIN;
    gpio_init.PinMode = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(USER_LED_PORT, &gpio_init);
    GPIO_WriteBit(USER_LED_PORT, gpio_init.Pins, 1u);
    GPIO_PinAFConf(USER_LED_PORT, gpio_init.Pins, GPIO_AF_15); /* disable the alternative functions. */
}

void LED_On(void)
{
    GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, 0u);
}

void LED_Off(void)
{
    GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, 1u);
}