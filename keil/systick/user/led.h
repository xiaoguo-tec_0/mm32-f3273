#ifndef __LED_H__
#define __LED_H__

#include "hal_gpio.h"

#define USER_LED_PIN 	GPIO_PIN_1
#define USER_LED_PORT 	GPIOA

void LED_Init(void);
void LED_On(void);
void LED_Off(void);
#endif // __LED_H__