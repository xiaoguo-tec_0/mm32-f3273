#include "board_init.h"
#include "key.h"
#include "clock_init.h"
#include "led.h"
#include "uart.h"
#include "timer.h"

void BOARD_Init(void)
{
    BOARD_InitBootClocks();  
	BOARD_InitDebugConsole();
	BOARD_UserKeyInit();
    LED_Init();
	BOARD_TIM6_Init();
}