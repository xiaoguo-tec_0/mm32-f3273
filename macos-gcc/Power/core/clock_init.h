/*
 * Copyright 2021 MindMotion Microelectronics Co., Ltd.
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __CLOCK_INIT_H__
#define __CLOCK_INIT_H__

#include "hal_common.h"

#define CLOCK_SYS_FREQ         120000000u
#define CLOCK_SYSTICK_FREQ     (CLOCK_SYS_FREQ/8u)
#define CLOCK_AHB1_FREQ        120000000u
#define CLOCK_AHB2_FREQ        120000000u
#define CLOCK_AHB3_FREQ        120000000u
#define CLOCK_APB1_FREQ        60000000u
#define CLOCK_APB2_FREQ        60000000u

#define LSI_VALUE           (40000)
#define HSI_VALUE           (8000000)
#define HSE_VALUE           (12000000)


#define RCC_CFGR_SWS_HSI    0x00000000U                       /*!< HSI oscillator used as system clock */
#define RCC_CFGR_SWS_HSE    0x00000004U                       /*!< HSE oscillator used as system clock */
#define RCC_CFGR_SWS_PLL    0x00000008U                       /*!< PLL used as system clock */
#define RCC_CFGR_SWS_LSI    0x0000000CU                       /*!< LSI used as system clock */

#define RCC_SYSCLKSOURCE_STATUS_HSI         RCC_CFGR_SWS_HSI            /*!< HSI used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_HSE         RCC_CFGR_SWS_HSE            /*!< HSE used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_PLLCLK      RCC_CFGR_SWS_PLL            /*!< PLL used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_LSI         RCC_CFGR_SWS_LSI            /*!< LSI used as system clock */

void BOARD_InitBootClocks(void);
uint32_t CLOCK_GetBootHSEValue(void);
void CLOCK_ResetToDefault(void);
void CLOCK_BootToHSI96MHz(void);
void CLOCK_BootToHSE96MHz(void);
void CLOCK_BootToHSE120MHz(void);
void CLOCK_SetClock_2M(void);
#endif /* __CLOCK_INIT_H__ */

