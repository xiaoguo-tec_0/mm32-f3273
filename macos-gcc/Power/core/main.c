#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"
#include "multi_button.h"
#include "shell_port.h"
#include "board_init.h"
#include "power_mode.h"

/* Configure sleep mode parameters. */
static POWER_SleepConf_Type sleep_conf =
{
    .SleepOnExit = false,
    .WakeUpMode = POWER_WakeUp_Event,
};

int main(void)
{
	BOARD_Init();
	//User_Shell_Init();
	BOARD_Delay1Ms(1000);
	printf("in main\r\n");
	LED_Toggle();
	BOARD_Delay1Ms(1000);
	printf("in main\r\n");
	LED_Toggle();
	BOARD_Delay1Ms(1000);
	printf("in main\r\n");
	LED_Toggle();
	BOARD_Delay1Ms(1000);
	printf("in main\r\n");
	LED_Toggle();


	POWER_SelectMode(POWER_Mode_Sleep, &sleep_conf);


	while(1)
	{

		
	}	
}

