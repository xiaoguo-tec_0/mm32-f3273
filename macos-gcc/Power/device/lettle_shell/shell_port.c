#include "shell_port.h"

#include "shell.h"
#include "hal_uart.h"
#include "shell_port.h"

Shell shell;
char shell_buffer[512];

short userShellWrite(char *data, unsigned short len)
{
    uint32_t i;
	for (i = 0; i < len; i++)
	{
	    while ( 0u == (UART_STATUS_TX_EMPTY & UART_GetStatus(UART1)) )
        {}
        UART_PutData(UART1, (uint8_t)(data[i]));
	}
    return len;
}

void User_Shell_Init(void)
{
    shell.write = userShellWrite;
    shellInit(&shell, shell_buffer, 512);
}
