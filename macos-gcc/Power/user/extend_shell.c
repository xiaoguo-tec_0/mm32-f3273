#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "hal_power.h"
#include "power_mode.h"
#include "extend_shell.h"
#include "clock_init.h"

int set_power_mode(int argc,char *argv[])
{
    if (argc != 2)
    {
        printf("use error and use spower 0/1/2/3/4/5/6");
        return -1;
    }
    else
    {
        POWER_Mode_Type mode = (POWER_Mode_Type) strtol(argv[1],NULL,10);
        if (mode >= 0 && mode <= 6)
        {
            BOARD_PowerModeSelect(mode);
        }
        else
        {
            printf("use error and use spower 0/1/2/3/4/5/6");
            return -1; 
        }
    }

    return 0;
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN), spower, set_power_mode, set_power_mode);


int get_sys_clk(int argc,char *argv[])
{
    if (argc != 1)
    {
        printf("error ,please use gsysclk\r\n");
    }
    else
    {
        printf("get sys clk == %ld\r\n",CLOCK_GetBootHSEValue());
    }
    return 0;
}


SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN), gsysclk, get_sys_clk, get_sys_clk);