#include "power_mode.h"
#include "clock_init.h"
#include "uart.h"
#include "timer.h"
#include <stdio.h>
#include "systick.h"

/*POWER*/
#define BOARD_POWER_STANDBYWAKEUP_PORT      PWR_WAKEUP_PIN_5
#define BOARD_POWER_STANDBYWAKEUP_DELAY     PWR_StandbyWakeUpDelay_5
/* Configure sleep mode parameters. */
static POWER_SleepConf_Type sleep_conf =
{
    .SleepOnExit = false,
    .WakeUpMode = POWER_WakeUp_Interrupt,
};

/* Configure stop mode parameters. */
static POWER_StopConf_Type stop_conf =
{
    .DeepStop = false,
    .WakeUpMode = POWER_WakeUp_Interrupt,
};

/* Configure deep stop mode parameters. */
static POWER_StopConf_Type deep_stop_conf =
{
    .DeepStop = true,
    .WakeUpMode = POWER_WakeUp_Interrupt,
};

/* Configure standby mode wake up pin parameters. */
static PWR_StandbyWakeUpPinConf_Type WakeUpPinConf =
{
    .Pins = BOARD_POWER_STANDBYWAKEUP_PORT,
    .TrgIn = PWR_StandbyWakeUpPinTriggerIn_FallingEdge,
};

/* Configure standby mode parameters. */
static POWER_StandbyConf_Type standby_conf =
{
    .Delay = BOARD_POWER_STANDBYWAKEUP_DELAY,
    .WakeUpMode = POWER_WakeUp_Interrupt,
};

void BOARD_PowerModeSelect(POWER_Mode_Type mode)
{
    switch (mode)
    {
    case POWER_Mode_Run/* constant-expression */:
        /* code */
        CLOCK_ResetToDefault();
        BOARD_ResetDebugConsole(CLOCK_GetBootHSEValue());
        BOARD_Delay1Ms(100);
        BOARD_TIM6_Init();
        printf("\r\n");
        printf("\r\n Mcu is going in run mode ....\r\n");
        break;
    case POWER_Mode_LowPowerRun:
        CLOCK_SetClock_2M();
        BOARD_ResetDebugConsole(CLOCK_GetBootHSEValue());
        BOARD_TIM6_Init();
        BOARD_Delay1Ms(100);
        printf("\r\nMCU is going in low power run mode...\r\n");
        printf("get system clock == %ld\r\n",CLOCK_GetBootHSEValue());
        POWER_SelectMode(POWER_Mode_LowPowerRun, NULL);
        break;
    case POWER_Mode_Sleep:
        CLOCK_ResetToDefault();
        BOARD_ResetDebugConsole(CLOCK_GetBootHSEValue());
        printf("\r\nMCU is going in sleep mode...\r\n");
        POWER_SelectMode(POWER_Mode_Sleep, &sleep_conf);
        break;
    case POWER_Mode_LowPowerSleep:
        break;
    case POWER_Mode_Stop:
        break;
    case POWER_Mode_DeepStop:
        break;
    case POWER_Mode_Standby:
        break;                        
    
    default:
        break;
    }
}