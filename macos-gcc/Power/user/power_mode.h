#ifndef __POWER_MODE_H__
#define __POWER_MODE_H__
#include "hal_common.h"
#include "hal_power.h"


void BOARD_PowerModeSelect(POWER_Mode_Type mode);
#endif //__POWER_MODE_H__