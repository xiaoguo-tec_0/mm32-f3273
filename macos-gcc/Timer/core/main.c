#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"

/**
 * @brief 软件延时函数
 * 
 * @param x 延时时间
 */
void APP_Delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 10000u; j++)
        {
            __NOP();
        }
    }
}

/**
 * Tim6 的使用
*/
int main(void)
{
	BOARD_InitBootClocks();  // 初始化时钟
	BOARD_InitDebugConsole();
	BOARD_UserKeyInit();
	LED_Init();
	BOARD_TIM6_Init();

	BOARD_Delay1Ms(1000);

	printf("Board Init Success\r\n");
	while(1)
	{

	}	
}

