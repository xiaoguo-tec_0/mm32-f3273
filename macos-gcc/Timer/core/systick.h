#ifndef __SYSTICK_H__
#define __SYSTICK_H__
#include "hal_common.h"

#define USE_INTERRUPT_MODE  	0
#define USE_POLL_DIV8_MODE		1  // 8Div
#define USE_POLL_DIV1_MODE		2  // NO DIV

#define SYSTICK_MODE		USE_POLL_DIV1_MODE


void BOARD_Delay1Us(uint32_t count);
void BOARD_Delay1Ms(uint32_t count);

#if SYSTICK_MODE == USE_INTERRUPT_MODE
void BOARD_Delay_Decrement(void);
#endif

#endif