#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"
#include "multi_button.h"
#include "FreeRTOS.h"
#include "task.h"
#include "shell_port.h"

struct Button btn1;

void BTN1_PRESS_DOWN_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_DOWN_Handler \r\n");
}

void BTN1_PRESS_UP_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_UP_Handler \r\n");
}

/* Task 1:  */
void vTask_TASK1(void * pvParameters);

/* Task 2:  */
void vTask_TASK2(void * pvParameters);

/* Task 1: blink LED0. */
void vTask_TASK1(void * pvParameters)
{
    (void)pvParameters;

    while(1)
    {
		//printf("in task 1......\r\n");
        vTaskDelay(500);
    }
}

/* Task 2: blink LED1. */
void vTask_TASK2(void * pvParameters)
{
    (void)pvParameters;
    while(1)
    {
		//printf("in task 2......\r\n");
        vTaskDelay(1000); 
    }
}

int main(void)
{
	BOARD_InitBootClocks(); 
	BOARD_InitDebugConsole();
	BOARD_UserKeyInit();
	LED_Init();
#if SYSTICK_MODE == USE_INTERRUPT_MODE	
	BOARD_Systick_Init(CLOCK_GetBootHSEValue()/configTICK_RATE_HZ);
#endif
	BOARD_TIM6_Init();

	// for mutilbutton init
	button_init(&btn1, read_button_GPIO, 0, btn1_id);
	button_attach(&btn1, PRESS_DOWN,       BTN1_PRESS_DOWN_Handler);
	button_attach(&btn1, PRESS_UP,         BTN1_PRESS_UP_Handler);
	button_start(&btn1);

	BOARD_TIM7_Init();

	printf("Board Init Success\r\n");
	User_Shell_Init();
	/* Create task. */
    xTaskCreate(vTask_TASK2, "TASK2", 128, NULL, 3, NULL);
    xTaskCreate(vTask_TASK1, "TASK1", 128, NULL, 4, NULL);

    /* Start scheduler. */
    vTaskStartScheduler();
	while(1)
	{

	}	
}

