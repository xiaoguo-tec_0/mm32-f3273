#include "hal_common.h"
#include "systick.h"
#include "FreeRTOS.h"
#include "task.h"

/**
 * 滴答定时器的中断
*/
void SysTick_Handler(void)
{
	#if SYSTICK_MODE == USE_INTERRUPT_MODE
	if(xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED)//系统已经运行
    {
       xPortSysTickHandler();
    }
    else
    {
		BOARD_Delay_Decrement();
	}
	#endif
}


