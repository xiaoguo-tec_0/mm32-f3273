#ifndef __BOARD_INIT_H__
#define __BOARD_INIT_H__

#include <stdio.h>

#include "hal_common.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "hal_i2c.h"

#include "clock_init.h"
#include "pin_init.h"

void BOARD_Init(void) ;
#endif // __BOARD_INIT_H__