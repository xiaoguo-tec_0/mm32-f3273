#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"
#include "multi_button.h"
#include "i2c.h"
#include "msa311.h"
#include "board_init.h"

struct Button btn1;

void BTN1_PRESS_DOWN_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_DOWN_Handler \r\n");
}

void BTN1_PRESS_UP_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_UP_Handler \r\n");
}

int main(void)
{
	BOARD_InitBootClocks(); 
	BOARD_InitDebugConsole();
	BOARD_UserKeyInit();

	LED_Init();
	BOARD_TIM6_Init();

	// for mutilbutton init
	button_init(&btn1, read_button_GPIO, 0, btn1_id);
	button_attach(&btn1, PRESS_DOWN,       BTN1_PRESS_DOWN_Handler);
	button_attach(&btn1, PRESS_UP,         BTN1_PRESS_UP_Handler);
	button_start(&btn1);

	BOARD_TIM7_Init();

	BOARD_I2C1_Init();
	BOARD_Delay1Ms(1000);

	int id = 0;
	bool ret = 0;
	int reg = 0x01;

#if DEBUG
#if I2C_MODE == USE_HARD_I2C  
	ret= BOARD_I2c1_Write(1,&reg);
	ret= BOARD_I2c1_Read(1,&id);
	printf("get msa311 partid == 0x%02x\r\n",id);
#elif I2C_MODE == USER_SW_I2C  

	SwI2c_Search_Device_Addr();
	SwI2c_Device_Read_Data(0xC4,&reg,1,&id,1);
	printf("get msa311 partid == 0x%02x\r\n",id);
#endif
#endif

	printf("Board Init Success\r\n");
	SwI2c_Search_Device_Addr();
	Msa311_Init();
	BOARD_InitPins();	

	while(1)
	{
		Msa311_GetXYZData();
        BOARD_Delay1Ms(1000);
	}	
}

