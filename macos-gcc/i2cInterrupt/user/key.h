#ifndef __KEY_H__
#define __KEY_H__
#include "hal_common.h"
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "hal_syscfg.h"
#include "hal_exti.h"


enum Button_IDs {
	btn1_id,
	btn2_id,
};

#define USER_KEY_PIN             GPIO_PIN_0
#define USER_KEY_PORT            GPIOA
#define USER_KEY                 GPIO_ReadInDataBit(USER_KEY_PORT, USER_KEY_PIN)
#define USER_KEY_GPIO_CLK_EN()   RCC_EnableAHB1Periphs(RCC_AHB1_PERIPH_GPIOA, true)
#define USER_SYSCFG_CLK_EN()     RCC_EnableAPB2Periphs(RCC_APB2_PERIPH_SYSCFG, true);

/*SYSCFG*/
#define BOARD_SYSCFG_EXTIPORT        SYSCFG_EXTIPort_GPIOA
#define BOARD_SYSCFG_EXTILINE        SYSCFG_EXTILine_0

/*EXTI*/
#define BOARD_EXTI_PORT              EXTI
#define BOARD_EXTI_LINE              EXTI_LINE_0
#define BOARD_EXTI_IRQN              EXTI0_IRQn
#define BOARD_EXTI_IRQHandler        EXTI0_IRQHandler

void BOARD_UserKeyInit(void);
uint8_t read_button_GPIO(uint8_t button_id);
#endif