#include "led.h"

void LED_Init(void)
{
    /* gpio. */
    GPIO_Init_Type gpio_init;

    gpio_init.Pins = USER_LED_PIN;
    gpio_init.PinMode = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(USER_LED_PORT, &gpio_init);
    GPIO_WriteBit(USER_LED_PORT, gpio_init.Pins, 1u);
    GPIO_PinAFConf(USER_LED_PORT, gpio_init.Pins, GPIO_AF_15); /* disable the alternative functions. */
    LED_On();
}

void LED_On(void)
{
    GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, 0u);
}

void LED_Off(void)
{
    GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, 1u);
}

void LED_Toggle(void)
{
    uint32_t odr;
    /* get current Output Data Register value */
    odr = USER_LED_PORT->ODR;

    /* Set selected pins that were at low level, and reset ones that were high */
    USER_LED_PORT->BSRR = ((odr & USER_LED_PIN) << 16U) | (~odr & USER_LED_PIN);
}