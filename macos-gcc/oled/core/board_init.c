
#include "board_init.h"
#include "hal_gpio.h"


/*
* Functions.
*/
void BOARD_Init(void)
{
    BOARD_InitBootClocks();
    BOARD_InitPins();
}