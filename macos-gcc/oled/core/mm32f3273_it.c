#include "hal_common.h"
#include "systick.h"
#include "hal_syscfg.h"
#include "hal_exti.h"
#include "msa311.h"
#include <stdio.h>
/**
 * 滴答定时器的中断
*/
void SysTick_Handler(void)
{
	#if SYSTICK_MODE == USE_INTERRUPT_MODE
	BOARD_Delay_Decrement();
	#endif
}

void EXTI9_5_IRQHandler(void)
{
	uint32_t flags = EXTI_GetLineStatus(EXTI);
    if ( 0u != ( flags & EXTI_LINE_8 ) ) /* Interrupts. */
    {
		handle_int_message();
    }
    EXTI_ClearLineStatus(EXTI, flags);
}


