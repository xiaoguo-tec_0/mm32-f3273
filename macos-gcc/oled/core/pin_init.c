#include "pin_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_syscfg.h"
#include "hal_exti.h"
#include "i2c.h"
#include "msa311.h"

void BOARD_InitPins(void)
{
    /* 调用GPIO的HAL库对象结构体声明GPIO对象 */
    GPIO_Init_Type gpio_init;

    //pc 8 for msa311 inter detect
    uint32_t prioritygroup = 0;
    RCC_EnableAPB2Periphs(RCC_APB2_PERIPH_SYSCFG, true);

    /* 设置GPIO的模式 */
    gpio_init.PinMode  = GPIO_PinMode_In_PullUp;
    gpio_init.Speed = GPIO_Speed_50MHz;
    
    /* 选择引脚初始化 */
    gpio_init.Pins  = GPIO_PIN_8;
    GPIO_Init(GPIOC, &gpio_init);

     /* 将外部中断线设置为复用模式 */
    SYSCFG_SetExtIntMux(SYSCFG_EXTIPort_GPIOC, SYSCFG_EXTILine_8);
    /* 设置外部中断线的触发方式 */
    EXTI_SetTriggerIn(EXTI, EXTI_LINE_8, EXTI_TriggerIn_BothEdges); 
    /* 使能外部中断线 */
    EXTI_EnableLineInterrupt(EXTI, EXTI_LINE_8, true);

    prioritygroup = NVIC_GetPriorityGrouping();
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, 0, 2));
    /* 使能对应的外部中断 */
    NVIC_EnableIRQ(EXTI9_5_IRQn);
}