#include "systick.h"
#include "clock_init.h"

#include "stdio.h"

#if SYSTICK_MODE == USE_POLL_DIV8_MODE

volatile static float count_1us = 0;
volatile static float count_1ms = 0;
/**
 *  初始化滴答定时器函数
 *  轮询方式延时
*/

void BOARD_Systick_Init()
{
	// Systick CTRL 寄存器 第二位为0时，Systick时钟==Sysclk/8 ,
	// 第二位为1时，Systick = Sysclk
	SysTick->CTRL &= ~(1<<2);// 将systick使用内核时钟，根据时钟树，即120/8=15M;
	
	count_1us = (float)(CLOCK_GetBootHSEValue()/8000000);
	count_1ms = (float)1000*count_1us;
}

void BOARD_Delay1Us(uint32_t count)
{
	uint32_t ctl;

	BOARD_Systick_Init();
	/* reload the count value */
    SysTick->LOAD = (uint32_t)(count * count_1us);
    /* clear the current count value */
    SysTick->VAL = 0x0000U;
    /* enable the systick timer */
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;
    /* wait for the COUNTFLAG flag set */
    do{
        ctl = SysTick->CTRL;
    }while((ctl&SysTick_CTRL_ENABLE_Msk)&&!(ctl & SysTick_CTRL_COUNTFLAG_Msk));
    /* disable the systick timer */
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    /* clear the current count value */
    SysTick->VAL = 0x0000U;
	
}

void BOARD_Delay1Ms(uint32_t count)
{
    uint32_t ctl;
    
	BOARD_Systick_Init();
    /* reload the count value */
    SysTick->LOAD = (uint32_t)(count * count_1ms);
    /* clear the current count value */
    SysTick->VAL = 0x0000U;
    /* enable the systick timer */
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;
    /* wait for the COUNTFLAG flag set */
    do{
        ctl = SysTick->CTRL;
    }while((ctl&SysTick_CTRL_ENABLE_Msk)&&!(ctl & SysTick_CTRL_COUNTFLAG_Msk));
    /* disable the systick timer */
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    /* clear the current count value */
    SysTick->VAL = 0x0000U;
}

#elif SYSTICK_MODE == USE_POLL_DIV1_MODE

uint32_t BOARD_Systick_Init(uint32_t ticks)
{ 
  if (ticks > SysTick_LOAD_RELOAD_Msk)  return (1);            /* Reload value impossible */
                                                               
  SysTick->LOAD  = (ticks & SysTick_LOAD_RELOAD_Msk) - 1;      /* set reload register */
  NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1);  /* set Priority for Cortex-M0 System Interrupts */
  SysTick->VAL   = 0;                                          /* Load the SysTick Counter Value */
  SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | 
                   SysTick_CTRL_ENABLE_Msk;                    /* Enable SysTick IRQ and SysTick Timer */
  SysTick->CTRL  &= ~SysTick_CTRL_TICKINT_Msk;
  return (0);                                                  /* Function successful */
}

void BOARD_Delay1Us( __IO uint32_t us)
{
	uint32_t i;	
	BOARD_Systick_Init(CLOCK_GetBootHSEValue() / 1000000);
	
	for(i=0;i<us;i++)
	{
		/* When the counter value decreases to 0, bit 16 of the CRTL register will be set to 1 */
		/* When set to 1, reading this bit will clear it to 0 */
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	/* Turn off the SysTick timer */
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}

void BOARD_Delay1Ms( __IO uint32_t ms)
{
	uint32_t i;	
	BOARD_Systick_Init(CLOCK_GetBootHSEValue() / 1000);
	
	for(i=0;i<ms;i++)
	{
		/* When the counter value decreases to 0, bit 16 of the CRTL register will be set to 1 */
		/* When set to 1, reading this bit will clear it to 0 */
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	/* Turn off the SysTick timer */
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}
#elif SYSTICK_MODE == USE_INTERRUPT_MODE

volatile static uint32_t delay;

uint32_t BOARD_Systick_Init(uint32_t ticks)
{ 
  if (ticks > SysTick_LOAD_RELOAD_Msk)  return (1);            /* Reload value impossible */
                                                               
  SysTick->LOAD  = (ticks & SysTick_LOAD_RELOAD_Msk) - 1;      /* set reload register */
  NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1);  /* set Priority for Cortex-M0 System Interrupts */
  SysTick->VAL   = 0;                                          /* Load the SysTick Counter Value */
  SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | 
                   SysTick_CTRL_ENABLE_Msk;                    /* Enable SysTick IRQ and SysTick Timer */
  SysTick->CTRL  |= SysTick_CTRL_TICKINT_Msk;
  return (0);                                                  /* Function successful */
}

void BOARD_Delay1Ms(uint32_t count)
{
    delay = count;

    while(0U != delay){
    }
}

void BOARD_Delay_Decrement(void)
{
    if (0U != delay){
        delay--;
    }
}


#endif

/* Software delay millisecond. */
void app_swdelay_ms(uint32_t ms)
{
    for (uint32_t i = 0u; i < ms; i++)
    {
        for (uint32_t j = 0u; j < (CLOCK_SYS_FREQ / 1000u); j++)
        {
            __NOP();
        }
    }
}

void app_swdelay_us(uint32_t us)
{
    for (uint32_t j = 0u; j < (CLOCK_SYS_FREQ / 1000000u); j++)
    {
        __NOP();
    } 
}
