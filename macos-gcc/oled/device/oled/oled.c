#include "hal_common.h"
#include "i2c.h"
#include "msa311.h"
#include "systick.h"
#include "oled.h"
#include <stdio.h>

static unsigned char OLED_buffer[1024] = {0};
const unsigned char F8X16[]=	  
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,// 0
  0x00,0x00,0x00,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00,0x18,0x18,0x00,0x00,//! 1
  0x00,0x12,0x36,0x24,0x48,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//" 2
  0x00,0x00,0x00,0x24,0x24,0x24,0xFE,0x48,0x48,0x48,0xFE,0x48,0x48,0x48,0x00,0x00,//# 3
  0x00,0x00,0x10,0x38,0x54,0x54,0x50,0x30,0x1C,0x14,0x14,0x54,0x54,0x38,0x10,0x10,//$ 4
  0x00,0x00,0x00,0x44,0xA4,0xA8,0xA8,0xA8,0x54,0x1A,0x2A,0x2A,0x2A,0x44,0x00,0x00,//% 5
  0x00,0x00,0x00,0x30,0x48,0x48,0x48,0x50,0x6E,0xA4,0x94,0x88,0x89,0x76,0x00,0x00,//& 6
  0x00,0x60,0x60,0x20,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//' 7
  0x00,0x02,0x04,0x08,0x08,0x10,0x10,0x10,0x10,0x10,0x10,0x08,0x08,0x04,0x02,0x00,//( 8
  0x00,0x40,0x20,0x10,0x10,0x08,0x08,0x08,0x08,0x08,0x08,0x10,0x10,0x20,0x40,0x00,//) 9
  0x00,0x00,0x00,0x00,0x10,0x10,0xD6,0x38,0x38,0xD6,0x10,0x10,0x00,0x00,0x00,0x00,//* 10
  0x00,0x00,0x00,0x00,0x10,0x10,0x10,0x10,0xFE,0x10,0x10,0x10,0x10,0x00,0x00,0x00,//+ 11
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x60,0x60,0x20,0xC0,//, 12
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//- 13
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x60,0x60,0x00,0x00,//. 14
  0x00,0x00,0x01,0x02,0x02,0x04,0x04,0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x40,0x00,/// 15
  0x00,0x00,0x00,0x18,0x24,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x24,0x18,0x00,0x00,//0 16
  0x00,0x00,0x00,0x10,0x70,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x7C,0x00,0x00,//1 17
  0x00,0x00,0x00,0x3C,0x42,0x42,0x42,0x04,0x04,0x08,0x10,0x20,0x42,0x7E,0x00,0x00,//2 18
  0x00,0x00,0x00,0x3C,0x42,0x42,0x04,0x18,0x04,0x02,0x02,0x42,0x44,0x38,0x00,0x00,//3 19
  0x00,0x00,0x00,0x04,0x0C,0x14,0x24,0x24,0x44,0x44,0x7E,0x04,0x04,0x1E,0x00,0x00,//4 20
  0x00,0x00,0x00,0x7E,0x40,0x40,0x40,0x58,0x64,0x02,0x02,0x42,0x44,0x38,0x00,0x00,//5 21
  0x00,0x00,0x00,0x1C,0x24,0x40,0x40,0x58,0x64,0x42,0x42,0x42,0x24,0x18,0x00,0x00,//6 22
  0x00,0x00,0x00,0x7E,0x42,0x44,0x08,0x08,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x00,//7 23
  0x00,0x00,0x00,0x3C,0x42,0x42,0x42,0x24,0x18,0x24,0x42,0x42,0x42,0x3C,0x00,0x00,//8 24
  0x00,0x00,0x00,0x18,0x24,0x42,0x42,0x42,0x26,0x1A,0x02,0x02,0x24,0x38,0x00,0x00,//9 25
  0x00,0x00,0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00,//: 26
  0x00,0x00,0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00,0x18,0x18,0x08,0x30,0x00,//; 27
  0x00,0x00,0x00,0x02,0x04,0x08,0x10,0x20,0x40,0x20,0x10,0x08,0x04,0x02,0x00,0x00,//< 28
  0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0x00,0x00,0x00,0xFE,0x00,0x00,0x00,0x00,0x00,//= 29
  0x00,0x00,0x00,0x40,0x20,0x10,0x08,0x04,0x02,0x04,0x08,0x10,0x20,0x40,0x00,0x00,//> 30
  0x00,0x00,0x00,0x3C,0x42,0x42,0x62,0x02,0x04,0x08,0x08,0x00,0x18,0x18,0x00,0x00,//? 31
  0x00,0x00,0x00,0x38,0x44,0x5A,0xAA,0xAA,0xAA,0xAA,0xB4,0x42,0x44,0x38,0x00,0x00,//@ 32
  0x00,0x00,0x00,0x10,0x10,0x18,0x28,0x28,0x24,0x3C,0x44,0x42,0x42,0xE7,0x00,0x00,//A 33
  0x00,0x00,0x00,0xF8,0x44,0x44,0x44,0x78,0x44,0x42,0x42,0x42,0x44,0xF8,0x00,0x00,//B 34
  0x00,0x00,0x00,0x3E,0x42,0x42,0x80,0x80,0x80,0x80,0x80,0x42,0x44,0x38,0x00,0x00,//C 35
  0x00,0x00,0x00,0xF8,0x44,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x44,0xF8,0x00,0x00,//D 36
  0x00,0x00,0x00,0xFE,0x42,0x48,0x48,0x78,0x48,0x48,0x40,0x42,0x42,0xFE,0x00,0x00,//E 37
  0x00,0x00,0x00,0xFE,0x42,0x48,0x48,0x78,0x48,0x48,0x40,0x40,0x40,0xE0,0x00,0x00,//F 38
  0x00,0x00,0x00,0x3C,0x44,0x44,0x80,0x80,0x80,0x8E,0x84,0x44,0x44,0x38,0x00,0x00,//G 39
  0x00,0x00,0x00,0xE7,0x42,0x42,0x42,0x42,0x7E,0x42,0x42,0x42,0x42,0xE7,0x00,0x00,//H 40
  0x00,0x00,0x00,0x7C,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x7C,0x00,0x00,//I 41
  0x00,0x00,0x00,0x3E,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x88,0xF0,//J 42
  0x00,0x00,0x00,0xEE,0x44,0x48,0x50,0x70,0x50,0x48,0x48,0x44,0x44,0xEE,0x00,0x00,//K 43
  0x00,0x00,0x00,0xE0,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x42,0xFE,0x00,0x00,//L 44
  0x00,0x00,0x00,0xEE,0x6C,0x6C,0x6C,0x6C,0x54,0x54,0x54,0x54,0x54,0xD6,0x00,0x00,//M 45
  0x00,0x00,0x00,0xC7,0x62,0x62,0x52,0x52,0x4A,0x4A,0x4A,0x46,0x46,0xE2,0x00,0x00,//N 46
  0x00,0x00,0x00,0x38,0x44,0x82,0x82,0x82,0x82,0x82,0x82,0x82,0x44,0x38,0x00,0x00,//O 47
  0x00,0x00,0x00,0xFC,0x42,0x42,0x42,0x42,0x7C,0x40,0x40,0x40,0x40,0xE0,0x00,0x00,//P 48
  0x00,0x00,0x00,0x38,0x44,0x82,0x82,0x82,0x82,0x82,0xB2,0xCA,0x4C,0x38,0x06,0x00,//Q 49
  0x00,0x00,0x00,0xF8,0x44,0x44,0x44,0x78,0x48,0x48,0x44,0x44,0x42,0xE3,0x00,0x00,//R 50
  0x00,0x00,0x00,0x3E,0x42,0x42,0x40,0x20,0x18,0x04,0x02,0x42,0x42,0x7C,0x00,0x00,//S 51
  0x00,0x00,0x00,0xFE,0x92,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x38,0x00,0x00,//T 52
  0x00,0x00,0x00,0xE7,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x3C,0x00,0x00,//U 53
  0x00,0x00,0x00,0xE7,0x42,0x42,0x44,0x24,0x24,0x28,0x28,0x18,0x10,0x10,0x00,0x00,//V 54
  0x00,0x00,0x00,0xD6,0x92,0x92,0x92,0x92,0xAA,0xAA,0x6C,0x44,0x44,0x44,0x00,0x00,//W 55
  0x00,0x00,0x00,0xE7,0x42,0x24,0x24,0x18,0x18,0x18,0x24,0x24,0x42,0xE7,0x00,0x00,//X 56
  0x00,0x00,0x00,0xEE,0x44,0x44,0x28,0x28,0x10,0x10,0x10,0x10,0x10,0x38,0x00,0x00,//Y 57
  0x00,0x00,0x00,0x7E,0x84,0x04,0x08,0x08,0x10,0x20,0x20,0x42,0x42,0xFC,0x00,0x00,//Z 58
  0x00,0x1E,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x1E,0x00,//[ 59
  0x00,0x00,0x40,0x40,0x20,0x20,0x10,0x10,0x08,0x04,0x04,0x02,0x02,0x02,0x01,0x01,//\ 60
  0x00,0x78,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x78,0x00,//] 61
  0x00,0x08,0x14,0x22,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//^ 62
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,//_ 63
  0x00,0x60,0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//` 64
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3C,0x42,0x1E,0x22,0x42,0x42,0x3F,0x00,0x00,//a 65
  0x00,0x00,0x00,0xC0,0x40,0x40,0x40,0x58,0x64,0x42,0x42,0x42,0x64,0x58,0x00,0x00,//b 66
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1C,0x22,0x40,0x40,0x40,0x22,0x1C,0x00,0x00,//c 67
  0x00,0x00,0x00,0x06,0x02,0x02,0x02,0x1E,0x22,0x42,0x42,0x42,0x26,0x1B,0x00,0x00,//d 68
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3C,0x42,0x7E,0x40,0x40,0x42,0x3C,0x00,0x00,//e 69
  0x00,0x00,0x00,0x0F,0x11,0x10,0x10,0x7E,0x10,0x10,0x10,0x10,0x10,0x7C,0x00,0x00,//f 70
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3E,0x44,0x44,0x38,0x40,0x3C,0x42,0x42,0x3C,//g 71
  0x00,0x00,0x00,0xC0,0x40,0x40,0x40,0x5C,0x62,0x42,0x42,0x42,0x42,0xE7,0x00,0x00,//h 72
  0x00,0x00,0x00,0x30,0x30,0x00,0x00,0x70,0x10,0x10,0x10,0x10,0x10,0x7C,0x00,0x00,//i 73
  0x00,0x00,0x00,0x0C,0x0C,0x00,0x00,0x1C,0x04,0x04,0x04,0x04,0x04,0x04,0x44,0x78,//j 74
  0x00,0x00,0x00,0xC0,0x40,0x40,0x40,0x4E,0x48,0x50,0x68,0x48,0x44,0xEE,0x00,0x00,//k 75
  0x00,0x00,0x00,0x70,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x7C,0x00,0x00,//l 76
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xF6,0x49,0x49,0x49,0x49,0x49,0xED,0x00,0x00,//m 77
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xDC,0x62,0x42,0x42,0x42,0x42,0xE7,0x00,0x00,//n 78
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3C,0x42,0x42,0x42,0x42,0x42,0x3C,0x00,0x00,//o 79
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xD8,0x64,0x42,0x42,0x42,0x64,0x58,0x40,0xE0,//p 80
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,0x22,0x42,0x42,0x42,0x26,0x1A,0x02,0x07,//q 81
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xEE,0x32,0x20,0x20,0x20,0x20,0xF8,0x00,0x00,//r 82
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3E,0x42,0x40,0x3C,0x02,0x42,0x7C,0x00,0x00,//s 83
  0x00,0x00,0x00,0x00,0x00,0x10,0x10,0x7C,0x10,0x10,0x10,0x10,0x10,0x0C,0x00,0x00,//t 84
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xC6,0x42,0x42,0x42,0x42,0x46,0x3B,0x00,0x00,//u 85
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xE7,0x42,0x24,0x24,0x28,0x10,0x10,0x00,0x00,//v 86
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xD7,0x92,0x92,0xAA,0xAA,0x44,0x44,0x00,0x00,//w 87
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x6E,0x24,0x18,0x18,0x18,0x24,0x76,0x00,0x00,//x 88
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xE7,0x42,0x24,0x24,0x28,0x18,0x10,0x10,0xE0,//y 89
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7E,0x44,0x08,0x10,0x10,0x22,0x7E,0x00,0x00,//z 90
  0x00,0x03,0x04,0x04,0x04,0x04,0x04,0x08,0x04,0x04,0x04,0x04,0x04,0x04,0x03,0x00,//{ 91
  0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,//| 92
  0x00,0x60,0x10,0x10,0x10,0x10,0x10,0x08,0x10,0x10,0x10,0x10,0x10,0x10,0x60,0x00,//} 93
  0x00,0x00,0x00,0x00,0x00,0x00,0x20,0x52,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//~ 94
};
const unsigned char F6x8[][8] =		
{
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // sp
	0x10,0x10,0x10,0x10,0x00,0x10,0x00,0x00, // !
	0x28,0x28,0x28,0x00,0x00,0x00,0x00,0x00, // "
	0x28,0x28,0x7C,0x28,0x7C,0x28,0x28,0x00, // #
	0x10,0x3C,0x50,0x38,0x14,0x78,0x10,0x00, // $
	0x00,0x4C,0x2C,0x10,0x08,0x60,0x60,0x00, // %
	0x30,0x48,0x50,0x20,0x54,0x48,0x34,0x00, // &
	0x30,0x10,0x20,0x00,0x00,0x00,0x00,0x00, // '
	0x08,0x10,0x20,0x20,0x20,0x10,0x08,0x00, // (
	0x20,0x10,0x08,0x08,0x08,0x10,0x20,0x00, // )
	0x00,0x10,0x54,0x38,0x54,0x10,0x00,0x00, // *
	0x00,0x10,0x10,0x7C,0x10,0x10,0x00,0x00, // +
	0x00,0x00,0x00,0x00,0x00,0x18,0x08,0x10, // ,
	0x00,0x00,0x00,0x00,0x7C,0x00,0x00,0x00, // -
	0x00,0x00,0x00,0x00,0x00,0x30,0x30,0x00, // .
	0x00,0x04,0x08,0x10,0x20,0x40,0x00,0x00, // /
	0x38,0x44,0x4C,0x54,0x64,0x44,0x38,0x00, // 0
	0x10,0x30,0x10,0x10,0x10,0x10,0x38,0x00, // 1
	0x38,0x44,0x04,0x08,0x10,0x20,0x7C,0x00, // 2
	0x7C,0x08,0x10,0x08,0x04,0x44,0x38,0x00, // 3 
	0x08,0x18,0x28,0x48,0x7C,0x08,0x08,0x00, // 4
	0x7C,0x40,0x78,0x04,0x04,0x44,0x38,0x00, // 5
	0x18,0x20,0x40,0x78,0x44,0x44,0x38,0x00, // 6
	0x7C,0x04,0x08,0x10,0x20,0x20,0x20,0x00, // 7
	0x38,0x44,0x44,0x38,0x44,0x44,0x38,0x00, // 8
	0x38,0x44,0x44,0x3C,0x04,0x08,0x30,0x00, // 9
	0x00,0x30,0x30,0x00,0x30,0x30,0x00,0x00, // :
	0x00,0x30,0x30,0x00,0x30,0x10,0x20,0x00, // ;
	0x08,0x10,0x20,0x40,0x20,0x10,0x08,0x00, // <
	0x00,0x00,0x7C,0x00,0x7C,0x00,0x00,0x00, // =
	0x20,0x10,0x08,0x04,0x08,0x10,0x20,0x00, // >
	0x38,0x44,0x04,0x08,0x10,0x00,0x10,0x00, // ?
	0x38,0x44,0x04,0x34,0x5C,0x44,0x38,0x00, // @
	0x10,0x28,0x44,0x44,0x7C,0x44,0x44,0x00, // A
	0x78,0x44,0x44,0x78,0x44,0x44,0x78,0x00, // B
	0x38,0x44,0x40,0x40,0x40,0x44,0x38,0x00, // C
	0x70,0x48,0x44,0x44,0x44,0x48,0x70,0x00, // D
	0x7C,0x40,0x40,0x78,0x40,0x40,0x7C,0x00, // E
	0x7C,0x40,0x40,0x78,0x40,0x40,0x40,0x00, // F
	0x38,0x44,0x40,0x5C,0x44,0x44,0x3C,0x00, // G
	0x44,0x44,0x44,0x7C,0x44,0x44,0x44,0x00, // H
	0x38,0x10,0x10,0x10,0x10,0x10,0x38,0x00, // I
	0x1C,0x08,0x08,0x08,0x08,0x48,0x30,0x00, // J
	0x44,0x48,0x50,0x60,0x50,0x48,0x44,0x00, // K
	0x40,0x40,0x40,0x40,0x40,0x40,0x7C,0x00, // L
	0x44,0x6C,0x54,0x54,0x44,0x44,0x44,0x00, // M
	0x44,0x44,0x64,0x54,0x4C,0x44,0x44,0x00, // N
	0x38,0x44,0x44,0x44,0x44,0x44,0x38,0x00, // O
	0x78,0x44,0x44,0x78,0x40,0x40,0x40,0x00, // P
	0x38,0x44,0x44,0x44,0x54,0x48,0x34,0x00, // Q
	0x78,0x44,0x44,0x78,0x50,0x48,0x44,0x00, // R
	0x3C,0x40,0x40,0x38,0x04,0x04,0x78,0x00, // S
	0x7C,0x10,0x10,0x10,0x10,0x10,0x10,0x00, // T
	0x44,0x44,0x44,0x44,0x44,0x44,0x38,0x00, // U
	0x44,0x44,0x44,0x44,0x44,0x28,0x10,0x00, // V
	0x44,0x44,0x44,0x54,0x54,0x54,0x28,0x00, // W
	0x44,0x44,0x28,0x10,0x28,0x44,0x44,0x00, // X
	0x44,0x44,0x44,0x28,0x10,0x10,0x10,0x00, // Y
	0x7C,0x04,0x08,0x10,0x20,0x40,0x7C,0x00, // Z
	0x38,0x20,0x20,0x20,0x20,0x20,0x38,0x00, // [
	0x00,0x40,0x20,0x10,0x08,0x04,0x00,0x00, // '\'
	0x38,0x08,0x08,0x08,0x08,0x08,0x38,0x00, // ]
	0x10,0x28,0x44,0x00,0x00,0x00,0x00,0x00, // ^
	0x00,0x00,0x00,0x00,0x00,0x00,0x7C,0x00, // _
	0x20,0x10,0x08,0x00,0x00,0x00,0x00,0x00, // '
	0x00,0x00,0x38,0x04,0x3C,0x44,0x3C,0x00, // a
	0x40,0x40,0x58,0x64,0x44,0x44,0x78,0x00, // b
	0x00,0x00,0x38,0x40,0x40,0x44,0x38,0x00, // c
	0x04,0x04,0x34,0x4C,0x44,0x44,0x3C,0x00, // d
	0x00,0x00,0x38,0x44,0x7C,0x40,0x38,0x00, // e
	0x38,0x24,0x20,0x70,0x20,0x20,0x20,0x00, // f
	0x00,0x00,0x3C,0x44,0x44,0x3C,0x04,0x38, // g
	0x40,0x40,0x58,0x64,0x44,0x44,0x44,0x00, // h
	0x10,0x00,0x30,0x10,0x10,0x10,0x38,0x00, // i
	0x08,0x00,0x18,0x08,0x08,0x08,0x48,0x30, // j
	0x40,0x40,0x48,0x50,0x60,0x50,0x48,0x00, // k
	0x30,0x10,0x10,0x10,0x10,0x10,0x38,0x00, // l
	0x00,0x00,0x68,0x54,0x54,0x44,0x44,0x00, // m
	0x00,0x00,0x58,0x64,0x44,0x44,0x44,0x00, // n
	0x00,0x00,0x38,0x44,0x44,0x44,0x38,0x00, // o
	0x00,0x00,0x58,0x64,0x64,0x58,0x40,0x40, // p
	0x00,0x00,0x34,0x4C,0x4C,0x34,0x04,0x04, // q
	0x00,0x00,0x58,0x64,0x40,0x40,0x40,0x00, // r
	0x00,0x00,0x3C,0x40,0x38,0x04,0x78,0x00, // s
	0x20,0x20,0x70,0x20,0x20,0x24,0x18,0x00, // t
	0x00,0x00,0x44,0x44,0x44,0x4C,0x34,0x00, // u
	0x00,0x00,0x44,0x44,0x44,0x28,0x10,0x00, // v
	0x00,0x00,0x44,0x44,0x54,0x54,0x28,0x00, // w
	0x00,0x00,0x44,0x28,0x10,0x28,0x44,0x00, // x
	0x00,0x00,0x44,0x44,0x44,0x3C,0x04,0x38, // y
	0x00,0x00,0x7C,0x08,0x10,0x20,0x7C,0x00, // z
	0x10,0x20,0x20,0x40,0x20,0x20,0x10,0x00, // {
	0x00,0x10,0x10,0x10,0x10,0x10,0x10,0x00, // |
	0x20,0x10,0x10,0x08,0x10,0x10,0x20,0x00, // }
	0x00,0x00,0x00,0x20,0x54,0x08,0x00,0x00, // ~
};

bool OLED_WirteCommand(uint8_t command)
{
    int reg = 0x00;
    return SwI2c_Device_Write_Data(OLED_DEVICE_ADDR,&reg,1,&command,1);
}

bool OLED_WirteData(uint8_t data)
{
    int reg = 0x40;
    return SwI2c_Device_Write_Data(OLED_DEVICE_ADDR,&reg,1,&data,1);
}

/*******************************************************************
 * @name       :void OLED_Display(void)
 * @date       :2018-08-27
 * @function   :Display in OLED screen
 * @parameters :None
 * @retvalue   :None
********************************************************************/  
void OLED_Display(void)
{
	uint8_t i,n;		    
	for(i=0;i<PAGE_SIZE;i++)  
	{  
		OLED_WirteCommand (YLevel+i);    //设置页地址（0~7）
		OLED_WirteCommand (XLevelL);      //设置显示位置—列低地址
		OLED_WirteCommand (XLevelH);      //设置显示位置—列高地址   
		for(n=0;n<WIDTH;n++)
		{
			OLED_WirteData(OLED_buffer[i*WIDTH+n]); 
		}
	}   //更新显示
}

static void OLED_Clear(unsigned dat)  
{  
	if(dat)
	{
		memset(OLED_buffer,0xff,sizeof(OLED_buffer));
	}
	else
	{
		memset(OLED_buffer,0,sizeof(OLED_buffer));
	}
	OLED_Display();
}

void OLED_Set_Pixel(unsigned char x, unsigned char y,unsigned char color)
{
	if(color)
	{
		OLED_buffer[(y/PAGE_SIZE)*WIDTH+x]|= (1<<(y%PAGE_SIZE))&0xff;
	}
	else
	{
		OLED_buffer[(y/PAGE_SIZE)*WIDTH+x]&= ~((1<<(y%PAGE_SIZE))&0xff);
	}
}		   			 


void GUI_ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t Char_Size,uint8_t mode)
{      	
	  unsigned char c=0,i=0,tmp,j=0;	
		c=chr-' ';//得到偏移后的值			
		if(x>WIDTH-1){x=0;y=y+2;}
		if(Char_Size ==16)
		{
			for(i=0;i<16;i++)
			{
			  if(mode)
				{
					tmp = F8X16[c*16+i];
				}
				else
				{
					tmp = ~(F8X16[c*16+i]);
				}
				for(j=0;j<8;j++)
				{
					if(tmp&(0x80>>j))
					{
						OLED_Set_Pixel(x+j, y+i,1);
					}
					else
					{
						OLED_Set_Pixel(x+j, y+i,0);
					}
				}
			}
		}
		else if(Char_Size==8)
		{	
				for(i=0;i<8;i++)
				{
				  if(mode)
					{
						tmp = F6x8[c][i];
					}
					else
					{
						tmp = ~(F6x8[c][i]);
					}
					for(j=0;j<8;j++)
					{
						if(tmp&(0x80>>j))
						{
							OLED_Set_Pixel(x+j, y+i,1);
						}
						else
						{
							OLED_Set_Pixel(x+j, y+i,0);
						}
					}
			}
	}
	else
	{
		return;
	}
	OLED_Display();
}

void GUI_ShowString(uint8_t x,uint8_t y,uint8_t *chr,uint8_t Char_Size,uint8_t mode)
{
	unsigned char j=0,csize;
	if(Char_Size == 16)
  {
	  csize = Char_Size/2;
	}
  else if(Char_Size == 8)
  {
	  csize = Char_Size/2+2;
	}
	else
	{
		return;
	}
	while (chr[j]!='\0')
	{		
		GUI_ShowChar(x,y,chr[j],Char_Size,mode);
		x+=csize;
		if(x>120)
		{
			x=0;
			y+=Char_Size;
		}
		j++;
	}
}



bool OLED_Init(void)
{
    /**************初始化SSD1306*****************/	
	OLED_WirteCommand(0xAE); /*display off*/
	OLED_WirteCommand(0x00); /*set lower column address*/
	OLED_WirteCommand(0x10); /*set higher column address*/
	OLED_WirteCommand(0x40); /*set display start line*/ 
	OLED_WirteCommand(0xB0); /*set page address*/
	OLED_WirteCommand(0x81); /*contract control*/ 
	OLED_WirteCommand(0xFF); /*128*/
	OLED_WirteCommand(0xA1); /*set segment remap*/ 
	OLED_WirteCommand(0xA6); /*normal / reverse*/
	OLED_WirteCommand(0xA8); /*multiplex ratio*/ 
	OLED_WirteCommand(0x3F); /*duty = 1/64*/
	OLED_WirteCommand(0xC8); /*Com scan direction*/
	OLED_WirteCommand(0xD3); /*set display offset*/ 
	OLED_WirteCommand(0x00);
	OLED_WirteCommand(0xD5); /*set osc division*/ 
	OLED_WirteCommand(0x80);
	OLED_WirteCommand(0xD9); /*set pre-charge period*/ 
	OLED_WirteCommand(0XF1);
	OLED_WirteCommand(0xDA); /*set COM pins*/ 
	OLED_WirteCommand(0x12);
	OLED_WirteCommand(0xDB); /*set vcomh*/ 
	OLED_WirteCommand(0x30);
	OLED_WirteCommand(0x8D); /*set charge pump disable*/ 
	OLED_WirteCommand(0x14);
	OLED_WirteCommand(0xAF); /*display ON*/

    OLED_Clear(0);

    GUI_ShowString(50,0,"JAY",16,1);

}

