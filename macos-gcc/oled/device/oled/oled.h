#ifndef __OLED_H__
#define __OLED_H__

#define OLED_DEVICE_ADDR  0x78

//--------------OLED参数定义---------------------
#define PAGE_SIZE       8
#define XLevelL		    0x00
#define XLevelH		    0x10
#define YLevel          0xB0
#define	Brightness	    0xFF 
#define WIDTH 	        128
#define HEIGHT 	        64	

bool OLED_Init(void);

#endif