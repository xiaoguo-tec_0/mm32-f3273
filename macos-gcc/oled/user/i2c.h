#ifndef __I2C_H__
#define __I2C_H__

#define USE_HARD_I2C    0
#define USER_SW_I2C     1

#define I2C_MODE        USER_SW_I2C

#if I2C_MODE == USER_SW_I2C
// PC6 scl PC7 sda
#define SWI2C1_SCL_PORT     GPIOC
#define SWI2C1_SCL_PIN      GPIO_PIN_6
#define SWI2C1_SCL_HIGH 	GPIO_WriteBit(SWI2C1_SCL_PORT,SWI2C1_SCL_PIN,1u)
#define SWI2C1_SCL_LOW 		GPIO_WriteBit(SWI2C1_SCL_PORT,SWI2C1_SCL_PIN,0u)

#define SWI2C1_SDA_PORT     GPIOC
#define SWI2C1_SDA_PIN      GPIO_PIN_7
#define SWI2C1_SDA_HIGH 	GPIO_WriteBit(SWI2C1_SDA_PORT,SWI2C1_SDA_PIN,1u)
#define SWI2C1_SDA_LOW		GPIO_WriteBit(SWI2C1_SDA_PORT,SWI2C1_SDA_PIN,0u)
#define SWI2C1_SDA_READ     GPIO_ReadInDataBit(SWI2C1_SDA_PORT,SWI2C1_SDA_PIN)
#define BIT0     0x01
#define BIT7     0x80
#endif

void BOARD_I2C1_Init(void);
#if I2C_MODE == USE_HARD_I2C
bool BOARD_I2c1_Write(uint8_t txlen, uint8_t *txbuf);
bool BOARD_I2c1_Read(uint8_t rxlen, uint8_t *rxbuf);
#elif  I2C_MODE == USER_SW_I2C

bool SwI2c_Device_Write_Data(uint8_t device_addr,uint8_t *reg_addr,
                                    uint16_t reg_len,const uint8_t *buf,uint8_t len);
bool SwI2c_Device_Read_Data(uint8_t device_addr,uint8_t *reg_addr,
                                    uint16_t reg_len, uint8_t *buf,uint8_t len);
void SwI2c_Search_Device_Addr(void);
#endif //I2C_MODE == USE_HARD_I2C
#endif // __I2C_H__