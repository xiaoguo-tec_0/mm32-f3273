#ifndef __UART_H__
#define __UART_H__


#define CONSOLE_TX_GPIO_PIN   	GPIO_PIN_6
#define CONSOLE_RX_GPIO_PIN		GPIO_PIN_7
#define CONSOLE_GPIO_PORT 		GPIOB

#define BOARD_DEBUG_UART_PORT	UART1


void BOARD_InitDebugConsole(void);
#endif //__UART_H__