#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "hal_tim.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"
#include "multi_button.h"
#include "shell_port.h"

int set_pwm(int argc,char *argv[])
{
	if (argc == 2)
	{
		if (!strncmp(argv[1],"on",2))
		{
    		TIM_Start(TIM2);
			printf("PWM On\r\n");
		}
		else if(!strncmp(argv[1],"off",3))
		{
			TIM_Stop(TIM2);
			LED_Off();
			printf("PWM Off\r\n");
		}
		else
		{
			printf("error ,please use 'pwm freq xx(1-1000)' /pwm on /pwm off\r\n");
			return -1;
		}
		
	}
	else if (argc == 3)
	{
		if (!strncmp(argv[1],"freq",4))
		{
			uint16_t period = strtol(argv[2],NULL,10);
			if (period > 0 && period < 1000)
			{
				printf("Pwm duty cycle changed \r\n");
				TIM_Stop(TIM2);
				LED_Off();
				TIM_PutChannelValue(TIM2, TIM_CHN_2, period);/* Change duty cycle. */
				TIM_Start(TIM2);
			}
			else
			{
				printf("error ,please use 'pwm freq xx(1-1000)' /pwm on /pwm off\r\n");
				return -1;
			}

		}
		else
		{
			printf("error ,please use 'pwm freq xx(1-1000)' /pwm on /pwm off\r\n");
			return -1;
		}
	}
	else
	{
		printf("error ,please use 'pwm freq xx(1-1000)' /pwm on /pwm off\r\n");
		return -1;
	}

    return 0;
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN), pwm, set_pwm, set_pwm);

int main(void)
{
	BOARD_InitBootClocks();  
	BOARD_InitDebugConsole();
	TIM2_Config();
	User_Shell_Init();
	while(1)
	{

	}	
}

