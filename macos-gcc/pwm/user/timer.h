#ifndef __TIMER_H__
#define __TIMER_H__

void BOARD_TIM6_Init(void);
void BOARD_TIM7_Init(void);
void TIM2_Config(void);
#endif // __TIMER_H__