#include "hal_common.h"
#include "systick.h"

/**
 * 滴答定时器的中断
*/
void SysTick_Handler(void)
{
	#if SYSTICK_MODE == USE_INTERRUPT_MODE
	BOARD_Delay_Decrement();
	#endif
}


