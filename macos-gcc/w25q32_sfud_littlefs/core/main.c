#include <stdint.h>
#include <stdio.h>
#include "clock_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "key.h"
#include "timer.h"
#include "multi_button.h"
#include "spi.h"
#include "sfud.h"
#include "lfs.h"

lfs_t app_lfs;
lfs_file_t app_lfs_file;
struct lfs_config app_lfs_config;

int lfs_flash_init(struct lfs_config *c);

struct Button btn1;

void BTN1_PRESS_DOWN_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_DOWN_Handler \r\n");
}

void BTN1_PRESS_UP_Handler(void* btn)
{
	//do something...
	printf("BTN1_PRESS_UP_Handler \r\n");
}

int main(void)
{
	BOARD_InitBootClocks();  // ��ʼ��ʱ��
	BOARD_InitDebugConsole();
	BOARD_UserKeyInit();
	LED_Init();
	BOARD_TIM6_Init();

	// for mutilbutton init
	button_init(&btn1, read_button_GPIO, 0, btn1_id);
	button_attach(&btn1, PRESS_DOWN,       BTN1_PRESS_DOWN_Handler);
	button_attach(&btn1, PRESS_UP,         BTN1_PRESS_UP_Handler);
	button_start(&btn1);

	BOARD_TIM7_Init();

	BOARD_Delay1Ms(1000);
	
	printf("Board Init Success\r\n");

    printf("spi_lfs_sfud_spi example.\r\n");

    int err = lfs_flash_init(&app_lfs_config);
    if (err)
    {
        printf("lfs_flash_init() failed.\r\n");
        while (1);
    }

    err = lfs_mount(&app_lfs, &app_lfs_config);
    if (err)
    {
        printf("lfs_mount() failed.\r\n");
        lfs_format(&app_lfs, &app_lfs_config);
        printf("lfs_format() done.\r\n");
        lfs_mount(&app_lfs, &app_lfs_config);
        printf("lfs_mount() done.\r\n");
    }

    // read current count
    uint32_t boot_count = 0;
    lfs_file_open(&app_lfs, &app_lfs_file, "boot_count", LFS_O_RDWR | LFS_O_CREAT);
    lfs_file_read(&app_lfs, &app_lfs_file, &boot_count, sizeof(boot_count));

    // update boot count
    boot_count += 1;
    lfs_file_rewind(&app_lfs, &app_lfs_file);
    lfs_file_write(&app_lfs, &app_lfs_file, &boot_count, sizeof(boot_count));

    // remember the storage is not updated until the file is closed successfully
    lfs_file_close(&app_lfs, &app_lfs_file);

    // release any resources we were using
    lfs_unmount(&app_lfs);

    // print the boot count
    printf("boot_count: %u\r\n", (unsigned)boot_count);

	while(1)
	{
	}	
}

